import {Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

    displayedColumns: string[] = ['date', 'gross', 'net_pay'];
    dataSource = new MatTableDataSource<any>([
        {
            date: 'August 24, 2019',
            gross: 'P 3,000.00',
            net_pay: 'P 2,615.00'
        },
        {
            date: 'August 17, 2019',
            gross: 'P 3,000.00',
            net_pay: 'P 2,820.00'
        },
        {
            date: 'August 10, 2019',
            gross: 'P 2,500.00',
            net_pay: 'P 2,150.00'
        },
        {
            date: 'August 3, 2019',
            gross: 'P 3,000.00',
            net_pay: 'P 2,295.00'
        },
        {
            date: 'July 27, 2019',
            gross: 'P 3,000.00',
            net_pay: 'P 2,680.00'
        }
    ]);

    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;

    constructor() {
    }

    ngOnInit(): void {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(filterValue: string) {
        console.log('tab3 filter ' + this.dataSource.filter);
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

}
