import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort, MatDialog} from '@angular/material';
import dayGridPlugin from '@fullcalendar/daygrid';
import {ApiService} from '../service/api.service';
import {DialogLeaveComponent} from '../dialog-leave/dialog-leave.component';
import {DialogCreditComponent} from '../dialog-credit/dialog-credit.component';


@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit, AfterViewInit {
    setView = 'attendance';

    displayedAttendanceColumns: string[] = ['date', 'status'];
    displayedLateColumns: string[] = ['date', 'approve', 'status'];
    displayedLeaveColumns: string[] = ['date', 'type', 'status'];
    displayedCreditColumns: string[] = ['date', 'amount', 'status'];

    dataAttendanceSource = new MatTableDataSource<any>();
    dataLateSource = new MatTableDataSource<any>();
    dataLeaveSource = new MatTableDataSource<any>();
    dataCreditSource = new MatTableDataSource<any>();

    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    calendarPlugins = [dayGridPlugin];
    calendarEventsOperation = [
        {
            title: 'Day Off',
            date: '2019-09-07',
            rendering: 'background',
            backgroundColor: '#ef1717',
            allDay: true
        },
        {
            title: 'Day Off',
            date: '2019-09-08',
            rendering: 'background',
            backgroundColor: '#ef1717',
            allDay: true
        }
    ];
    calendarEventsShift = [
        {
            title: 'Day Off',
            date: '2019-09-07',
            rendering: 'background',
            backgroundColor: '#ef1717',
            allDay: true
        },
        {
            title: 'Day Off',
            date: '2019-09-08',
            rendering: 'background',
            backgroundColor: '#ef1717',
            allDay: true
        }
    ];
    header = {
        left: 'title',
        center: '',
        right: 'prev, next'
    };

    filter: string;

    constructor(private apiService: ApiService,
                public dialog: MatDialog) {
    }

    changeContent(value: string) {
        this.setView = value;

        if (value === 'attendance') {
            this.dataAttendanceSource.paginator = this.paginator;
            this.getAttendanceData();
            this.clearFilters();
        } else if (value === 'ot') {
            this.dataLateSource.paginator = this.paginator;
            this.clearFilters();
            this.getLateData();
        } else if (value === 'leave') {
            this.dataLeaveSource.paginator = this.paginator;
            this.clearFilters();
            this.getLeaveData();
        } else {
            this.dataCreditSource.paginator = this.paginator;
            this.clearFilters();
            this.getCreditData();
        }
    }

    ngOnInit(): void {

        // setTimeout(() => {
        //     // this.clearFilters();
        //     this.getAttendanceData();
        //     this.getLateData();
        //     this.getLeaveData();
        //     this.getCreditData();
        //
        //     this.dataAttendanceSource.paginator = this.paginator;
        //     this.dataLateSource.paginator = this.paginator;
        //     this.dataLeaveSource.paginator = this.paginator;
        //     this.dataCreditSource.paginator = this.paginator;
        // }, 500);
    }

    clearFilters() {
        this.dataAttendanceSource.filter = '';
        this.dataLateSource.filter = '';
        this.dataLeaveSource.filter = '';
        this.dataCreditSource.filter = '';
        this.filter = '';
    }

    getAttendanceData() {
        this.apiService.getAttendanceData()
            .then(res => {
                this.dataAttendanceSource = JSON.parse(res.data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    getLateData() {
        this.apiService.getLateData()
            .then(res => {
                this.dataLateSource = JSON.parse(res.data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    getLeaveData() {
        this.apiService.getLeaveData()
            .then(res => {
                this.dataLeaveSource = JSON.parse(res.data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    getCreditData() {
        this.apiService.getCreditData()
            .then(res => {
                this.dataCreditSource = JSON.parse(res.data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    applyAttendanceFilter(filterValue: string) {
        this.dataAttendanceSource.filter = filterValue.trim().toLowerCase();
    }

    applyLateFilter(filterValue: string) {
        this.dataLateSource.filter = filterValue.trim().toLowerCase();
    }

    applyLeaveFilter(filterValue: string) {
        this.dataLeaveSource.filter = filterValue.trim().toLowerCase();
    }

    applyCreditFilter(filterValue: string) {
        this.dataLeaveSource.filter = filterValue.trim().toLowerCase();
    }

    openLeaveDialog() {
        this.dialog.open(DialogLeaveComponent, {panelClass: 'myapp-no-padding-dialog'});
    }

    openCreditDialog() {
        this.dialog.open(DialogCreditComponent, {panelClass: 'myapp-no-padding-dialog'});
    }

    ngAfterViewInit(): void {

        setTimeout(() => {
            // this.clearFilters();
            this.getAttendanceData();
            this.getLateData();
            this.getLeaveData();
            this.getCreditData();

            this.dataAttendanceSource.paginator = this.paginator;
            this.dataLateSource.paginator = this.paginator;
            this.dataLeaveSource.paginator = this.paginator;
            this.dataCreditSource.paginator = this.paginator;
        }, 500);
    }
}

