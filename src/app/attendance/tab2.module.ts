import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Tab2Page} from './tab2.page';
import {MaterialModule} from '../material.module';
import {FullCalendarModule} from '@fullcalendar/angular';
import {DialogCreditComponent} from '../dialog-credit/dialog-credit.component';
import {DialogLeaveComponent} from '../dialog-leave/dialog-leave.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{path: '', component: Tab2Page}]),
        MaterialModule,
        ReactiveFormsModule,
        FullCalendarModule,
    ],
    declarations: [Tab2Page]
})
export class Tab2PageModule {
}
