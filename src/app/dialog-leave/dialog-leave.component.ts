import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDatepickerInputEvent, MatDialog, MatDialogRef} from '@angular/material';
import {ToastController} from '@ionic/angular';

export interface Type {
    value: string;
    viewValue: string;
}

@Component({
    selector: 'app-dialog-leave',
    templateUrl: './dialog-leave.component.html',
    styleUrls: ['./dialog-leave.component.scss'],
})
export class DialogLeaveComponent implements OnInit {

    types: Type[] = [
        {value: 'type1', viewValue: 'Type 1'},
        {value: 'type2', viewValue: 'Type 2'},
        {value: 'type3', viewValue: 'Type 3'}
    ];

    @Output()
    dateChange: EventEmitter< MatDatepickerInputEvent< any>>;

    reason: string;
    leaveType: string;
    public myForm: FormGroup;
    private dateCount = 1;
    todayDate = new Date().toISOString().substring(0, 10);
    credits = 5;
    leaveFrom = '';
    dates: string[] = [];

    constructor(private formBuilder: FormBuilder,
                public toastController: ToastController,
                public dialogRef: MatDialogRef<DialogLeaveComponent>) {
        this.myForm = formBuilder.group({
            date: ['', Validators.required]
        });
    }

    ngOnInit() {
    }

    getDate(date: MatDatepickerInputEvent<Date>) {
        this.dates.push(date.value.toLocaleDateString().substring(0, 10));
    }

    selectType(value ) {
        this.leaveType = value;
    }

    addDate() {
        this.dateCount++;
        this.leaveFrom = 'addLeave';
        this.myForm.addControl('Date' + this.dateCount, new FormControl(''));
    }

    btnClose() {
        this.dialogRef.close();
    }

    btnSave() {
        console.log('LEAVE ' + this.todayDate + '\n' + this.leaveType +
            '\n' + this.dates + '\n'
            + this.credits + '\n' + this.reason);
        this.dialogRef.close();
        this.presentToast();
    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'Application sent',
            duration: 2000
        });
        await toast.present();
    }
}
