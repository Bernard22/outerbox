import {Component, OnInit} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-dialog-credit',
    templateUrl: './dialog-credit.component.html',
    styleUrls: ['./dialog-credit.component.scss'],
})
export class DialogCreditComponent implements OnInit {

    todayDate = new Date().toISOString().substring(0, 10);
    remainingBalance: number;
    reason: string;
    reqAmount: number;

    constructor(public toastController: ToastController,
                public dialogRef: MatDialogRef<DialogCreditComponent>) {
    }

    ngOnInit() {
        this.remainingBalance = 3000;
    }

    btnClose() {
        this.dialogRef.close();
    }

    btnSave() {

        if (this.remainingBalance < this.reqAmount) {
            this.presentToast('Insufficient Balance');
            return;
        }
        this.dialogRef.close();
        this.presentToast('Application Sent');
    }

    async presentToast(text) {
        const toast = await this.toastController.create({
            message: text,
            duration: 2000
        });
        await toast.present();
    }
}
