import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {MaterialModule} from '../material.module';

import {IonicModule} from '@ionic/angular';

import {Tab5Page} from './tab5.page';

const routes: Routes = [
    {
        path: '',
        component: Tab5Page
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        MaterialModule
    ],
    declarations: [Tab5Page]
})
export class Tab5PageModule {
}
