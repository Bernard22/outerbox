import {Component, OnInit} from '@angular/core';
import {ApiService} from '../service/api.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
    selector: 'app-tab5',
    templateUrl: './tab5.page.html',
    styleUrls: ['./tab5.page.scss'],
})

export class Tab5Page implements OnInit {

    profile: any;
    userData: any;
    userId = '';
    userName = '';
    isLoading = true;

    capturedSnapURL: string;
    cameraOptions: CameraOptions = {
        quality: 20,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    };

    constructor(private apiService: ApiService, private camera: Camera) {
    }

    ngOnInit(): void {
        this.getUserProfile();
    }

    private getUserProfile() {
        this.apiService.getProfileData()
            .then(res => {
                this.isLoading = false;
                this.profile = JSON.parse(res.data);
                this.userId = this.profile.userId;
                this.userName = this.profile.userName;
                this.userData = this.profile.userData;
            })
            .catch(error => {
                console.log(error);
            });
    }

    changeProfile() {
        this.camera.getPicture(this.cameraOptions).then((imageData) => {
            // this.camera.DestinationType.FILE_URI gives file URI saved in local
            // this.camera.DestinationType.DATA_URL gives base64 URI

            const base64Image = 'data:image/jpeg;base64,' + imageData;
            this.capturedSnapURL = base64Image;
        }, (err) => {

            console.log(err);
            // Handle error
        });
    }
}
