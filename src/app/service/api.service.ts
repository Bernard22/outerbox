import { Injectable } from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';

// @Injectable({
//   providedIn: 'root'
// })
export class ApiService {

    BASE_URL_HOME = 'https://api.myjson.com/bins/87o8v';
    RANKING_URL = 'https://api.myjson.com/bins/qc72l';
    ATTENDANCE_URL = 'https://api.myjson.com/bins/6s5jp';
    LATE_URL = 'https://api.myjson.com/bins/10t7r9';
    LEAVE_URL = 'https://api.myjson.com/bins/xu1qd';
    CREDIT_URL = 'https://api.myjson.com/bins/1gntkt';
    PROFILE_URL = 'https://api.myjson.com/bins/h50rt';

  constructor(private http: HTTP) { }

    getHomeData() {
        return this.http.get(this.BASE_URL_HOME, {}, {});
    }

    getRankingData() {
      return this.http.get(this.RANKING_URL, {}, {});
    }

    getAttendanceData() {
        return this.http.get(this.ATTENDANCE_URL, {}, {});
    }

    getLateData() {
        return this.http.get(this.LATE_URL, {}, {});
    }

    getLeaveData() {
        return this.http.get(this.LEAVE_URL, {}, {});
    }

    getCreditData() {
        return this.http.get(this.CREDIT_URL, {}, {});
    }

    getProfileData() {
        return this.http.get(this.PROFILE_URL, {}, {});
    }
}
