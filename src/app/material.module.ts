import {NgModule} from '@angular/core';
import {
    MatTableModule,
    MatStepperModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatListModule,
    MatTabsModule,
    MatDialogModule,
    MatNativeDateModule,
    MatProgressSpinnerModule
} from '@angular/material';


@NgModule({
    exports: [
        MatTableModule,
        MatStepperModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatOptionModule,
        MatSelectModule,
        MatPaginatorModule,
        MatSortModule,
        MatListModule,
        MatCardModule,
        MatTabsModule,
        MatDialogModule,
        MatNativeDateModule,
        MatProgressSpinnerModule,
    ]
})
export class MaterialModule {
}
