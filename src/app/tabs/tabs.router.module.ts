import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'bulletin',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../bulletin/tab1.module').then(m => m.Tab1PageModule)
                    }
                ]
            },
            {
                path: 'attendance',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../attendance/tab2.module').then(m => m.Tab2PageModule)
                    }
                ]
            },
            {
                path: 'payroll',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../payroll/tab3.module').then(m => m.Tab3PageModule)
                    }
                ]
            },
            {
                path: 'message',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../message/tab4.module').then(m => m.Tab4PageModule)
                    }
                ]
            },
            {
                path: 'profile',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../profile/tab5.module').then(m => m.Tab5PageModule)
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/bulletin',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/bulletin',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
