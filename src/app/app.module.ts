import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {HTTP} from '@ionic-native/http/ngx';
import {FullCalendarModule} from '@fullcalendar/angular';
import {MaterialModule} from './material.module';
import {DialogLeaveComponent} from './dialog-leave/dialog-leave.component';
import {DialogCreditComponent} from './dialog-credit/dialog-credit.component';
import {MatDatepickerModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import {DialogCreditViewComponent} from './dialog-credit-view/dialog-credit-view.component';
import {DialogLeaveViewComponent} from './dialog-leave-view/dialog-leave-view.component';

@NgModule({
    declarations: [
        AppComponent,
        DialogLeaveComponent,
        DialogCreditComponent,
        DialogCreditViewComponent,
        DialogLeaveViewComponent,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        BrowserAnimationsModule,
        MatTableModule,
        FullCalendarModule,
        MaterialModule,
        MatDatepickerModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {
            provide: RouteReuseStrategy,
            useClass: IonicRouteStrategy
        },
        HTTP,
        Camera
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        DialogLeaveComponent,
        DialogCreditComponent,
        DialogCreditViewComponent,
        DialogLeaveViewComponent,
    ],
})
export class AppModule {
}
