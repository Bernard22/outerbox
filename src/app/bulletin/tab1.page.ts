import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../service/api.service';
import {MatTab} from '@angular/material';

export interface ExampleTab {
    label: string;
    content: string;
}

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit, AfterViewInit {

    setView = 'home';
    homesData: any;
    rankingData: any;
    topEmployees: any;
    @ViewChild(MatTab, {static: false}) tabs;
    tabNum = 0;

    private SWIPE_ACTION = {
        LEFT: 'swipeleft',
        RIGHT: 'swiperight'
    };
    selectedIndex = 0;
    isLoading = true;

    constructor(private apiService: ApiService) {
    }

    changeContent(value: string) {
        this.setView = value;
        if (value === 'home') {
            this.isLoading = true;
            this.homesData = '';
            this.getHomeData();
        } else if (value === 'feed') {
            this.isLoading = true;
            this.homesData = '';
            this.getHomeData();
        } else if (value === 'badge') {
            this.isLoading = true;
            this.rankingData = '';
            this.getRankingData();
        } else if (value === 'ranking') {
            this.isLoading = true;
            this.rankingData = '';
            this.getRankingData();
        }
    }

    ngOnInit(): void {
        this.isLoading = true;
        this.setView = 'home';
        this.getHomeData();
        this.getRankingData();
    }

    getHomeData() {
        this.apiService.getHomeData()
            .then(res => {
                this.isLoading = false;
                this.homesData = JSON.parse(res.data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    applyFilter(filterValue: string, index: number) {
        // this.rankingData.filter = filterValue.trim().toLowerCase();
        return this.rankingData.filter(item => {

            const topEmployees = [];
            for (const i of item.top_employees) {
                topEmployees.push(i);
            }
            this.topEmployees = topEmployees;
            return this.topEmployees.name.toLowerCase().indexOf(filterValue.toLowerCase()) > 1;
        });
    }

    getRankingData() {
        this.apiService.getRankingData()
            .then(res => {
                this.rankingData = JSON.parse(res.data);

                const topEmployees = [];
                for (const i of this.rankingData) {
                    for (const y of i.top_employees) {
                        topEmployees.push(i);
                    }
                }
                this.isLoading = false;
                this.topEmployees = topEmployees;
            })
            .catch(error => {
               console.log(error);
            });
    }

    ngAfterViewInit(): void {
        this.tabNum = 4;
    }

    swipe(selectedIndex: number, action) {

        if (action === this.SWIPE_ACTION.LEFT) {
            this.selectedIndex = selectedIndex + 1;
        }

        if (action === this.SWIPE_ACTION.RIGHT) {
            this.selectedIndex = selectedIndex - 1;
        }
    }
}
